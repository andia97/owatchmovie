import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Movie';
  movie = {
    name: 'Liga de la Justicia',
    description: 'blabsldbala'
  };
  mostrarNgIf = false;
  movies = ['Avengers', 'Coco', 'Toy Story', 'Conjuro'];
  moviesObj = [
    {
      id: 1,
      name: 'Liga De la Justicia',
      genero: 'Accion/Ciencia Ficción',
      price: '10bs',
      imagen: 'https://i1.wp.com/hipertextual.com/wp-content/uploads/2021/01/Zack-Snyder-Justice-League.jpg?fit=2107%2C1328&ssl=1'
    },
    {
      id: 2,
      name: 'Batman',
      genero: 'Acción',
      price: '10bs',
      imagen: 'https://spoiler.bolavip.com/__export/1601992377555/sites/bolavip/img/2020/10/06/batman_reeves_crop1601992270723.jpg_2011752842.jpg'
    },
    {
      id: 3,
      name: 'Up',
      genero: 'Infantil',
      price: '10bs',
      imagen: 'https://www.funeralnatural.net/sites/default/files/pelicula/imagen/upcartel.jpg'
    },
    {
      id: 4,
      name: 'Toy Story',
      genero: 'Infantil',
      price: '10bs',
      imagen: 'https://i.blogs.es/a34e58/cartel-toy-story-4/840_560.jpg'
    },
  ];
  mostrar(): void {
    this.mostrarNgIf = true;
  }
  ocultar(): void {
    this.mostrarNgIf = false;
  }
  addMovie(): void {
    this.movies.push('New Movie');
  }
  deleteMovie(index: number): void {
    this.movies.splice(index, 1);
  }
}
